#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>

#include <vector>
#include <algorithm>
#include <string>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Vertex Vertex;
typedef Polyhedron::Halfedge_around_facet_const_circulator Halfedge_facet_circulator;
typedef Polyhedron::Point_3 Point3;






//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Structures et variables globales ////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

/// @brief Nombre maximum de points qu'une feuille de l'octree peut contenir
int MAX_POINT = 35;

/// @brief Profondeur maximum de l'octree souhaitée
int MAX_DEPTH = 10;

/// @brief Axis-Aligned bounding box
/// Structure représentant une boîte englobante
struct AABB
{
	Point3 coin_min;
	Point3 coin_max;
    Point3 center;
    AABB() {}
	AABB(Point3 coin_min_in, Point3 coin_max_in) : coin_min(coin_min_in), coin_max(coin_max_in), center((coin_max_in.x() + coin_min_in.x()) / 2, (coin_max_in.y() + coin_min_in.y()) / 2, (coin_max_in.z() + coin_min_in.z()) / 2) {}
};

/// @brief Structure représentant un noeud de l'octree
struct OctreeNode
{
    AABB boundingBox;
	std::vector<Point3> liste_sommets;
    std::vector<OctreeNode> enfants;
	int depth = 0;
	OctreeNode() {}
	OctreeNode(AABB _boundingBox, int dpth) : boundingBox(_boundingBox), depth(dpth) {}
	Point3 mergedOctree_vertex;
	int simplifiedOctree_index = -1;
};

/// @brief (Pour débug) Affiche des informations utiles sur un octree
/// @param root L'octree à afficher
void printOctree(OctreeNode root) {
	if (root.liste_sommets.size() > 0) {
		std::cout << root.depth << " : " << root.liste_sommets.size();

		if (root.enfants.size() != 0) {
			if (root.liste_sommets.size() > 0) {
				std::cout << "\tShould be 0";
			}
		}

		std::cout << "\t" << root.simplifiedOctree_index;
		std::cout << "\t(" << root.mergedOctree_vertex.x() << ", " << root.mergedOctree_vertex.y() << ", " << root.mergedOctree_vertex.z() << ")";
		std::cout << std::endl;
	}

	for (auto node : root.enfants) {
		printOctree(node);
	}
}






//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Construction d'un octree ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

/// @brief Compute the bounding box of a mesh
/// @param mesh the mesh of interest
/// @return its bounding box
AABB computeBB(const Polyhedron &mesh)
{
    auto i = mesh.vertices_begin();
    Point3 currentPoint = i->point();

    double xMin, yMin, zMin, xMax, yMax, zMax;
    xMin = (currentPoint).x();
    yMin = (currentPoint).y();
    zMin = (currentPoint).z();
    xMax = (currentPoint).x();
    yMax = (currentPoint).y();
    zMax = (currentPoint).z();
    
    for(i = mesh.vertices_begin(); i != mesh.vertices_end(); i++) {
        currentPoint = i->point();
        if(currentPoint.x() < xMin) xMin = currentPoint.x();
        if(currentPoint.x() > xMax) xMax = currentPoint.x();

        if(currentPoint.y() < yMin) yMin = currentPoint.y();
        if(currentPoint.y() > yMax) yMax = currentPoint.y();

        if(currentPoint.z() < zMin) zMin = currentPoint.z();
        if(currentPoint.z() > zMax) zMax = currentPoint.z();
    }

    std::cout << "========= Computing mesh bounding box =========" << std::endl;
    std::cout << "Coin min : (" << xMin << ", " << yMin << ", " << zMin << ")" << std::endl;
    std::cout << "Coin max : (" << xMax << ", " << yMax << ", " << zMax << ")" << std::endl;

	return AABB{Point3(xMin, yMin, zMin), Point3(xMax, yMax, zMax)};
}

/// @brief add a level to the given parent Octree node, by creating 8 children with 8 bounding box,
/// sliced in the middle of the parent node
/// @param node the octree node to which 8 children will be added
void addOctreeLevel(OctreeNode &node)
{
    Point3 coin_min = node.boundingBox.coin_min;
    Point3 coin_max = node.boundingBox.coin_max;
    Point3 center = node.boundingBox.center;
	std::cout << "Ajout d'un niveau à l'octree" << std::endl;
	int depth = node.depth + 1;

    if(node.enfants.size() > 0) node.enfants.clear();
    
    node.enfants.push_back(OctreeNode{AABB{coin_min, center}, depth});
    node.enfants.push_back(OctreeNode{AABB{Point3(center.x(), coin_min.y(), coin_min.z()), Point3(coin_max.x(), center.y(), center.z())}, depth});
    node.enfants.push_back(OctreeNode{AABB{Point3(coin_min.x(), coin_min.y(), center.z()), Point3(center.x(), center.y(), coin_max.z())}, depth});
    node.enfants.push_back(OctreeNode{AABB{Point3(center.x(), coin_min.y(), center.z()), Point3(coin_max.x(), center.y(), coin_max.z())}, depth});

    node.enfants.push_back(OctreeNode{AABB{Point3(coin_min.x(), center.y(), coin_min.z()), Point3(center.x(), coin_max.y(), center.z())}, depth});
    node.enfants.push_back(OctreeNode{AABB{Point3(center.x(), center.y(), coin_min.z()), Point3(coin_max.x(), coin_max.y(), center.z())}, depth});
    node.enfants.push_back(OctreeNode{AABB{Point3(coin_min.x(), center.y(), center.z()), Point3(center.x(), coin_max.y(), coin_max.z())}, depth});
    node.enfants.push_back(OctreeNode{AABB{center, coin_max}, depth});
}

/// @brief Indique pour chaque coordonnée où le sommet se situe par rapport à l'axe médian de la boîte
/// @param vert Le sommet à localiser
/// @param boundingBox La boîte englobante considérée
/// @return Tableau de résultat
std::vector<bool> compareWithCenter(Point3 &vert, AABB boundingBox) {
    std::vector<bool> result;
    result.push_back(vert.x() > boundingBox.center.x());
    result.push_back(vert.y() > boundingBox.center.y());
    result.push_back(vert.z() > boundingBox.center.z());
    return result;
}

/// @brief Indique si un sommet se situe à l'intérieur d'uen bounding box
/// @param vert Le sommet en entrée
/// @param boundingBox La bounding box considérée
/// @return True si le sommet est dans la boîte, False sinon
bool isVertexInBoundingBox(Polyhedron::Vertex_handle &vert, AABB boundingBox) {
    if (vert->point().x() < boundingBox.coin_max.x() && vert->point().x() >= boundingBox.coin_min.x() &&
        vert->point().y() < boundingBox.coin_max.y() && vert->point().y() >= boundingBox.coin_min.y() &&
        vert->point().z() < boundingBox.coin_max.z() && vert->point().z() >= boundingBox.coin_min.z())
    {
        return true;
    }
    return false;
}

/// @brief Parmi les 8 noeuds enfants d'un OctreeNode, retourne celui qui contient le point recherché
/// Parcourt tous les enfants et regarde si le sommet est dans la bounding box correspondante.
/// @param root Le noeud parent
/// @param vert Le sommet recherché
/// @return Le noeud enfant contenant le sommet
OctreeNode& findBoundingBoxOfVertex_Basic(OctreeNode& root, Polyhedron::Vertex_handle &vert) {
	for (int i = 0; i < root.enfants.size(); i++)
	{
		if (isVertexInBoundingBox(vert, root.enfants.at(i).boundingBox)) {
			return root.enfants.at(i);
		}
	}
	return root;
}

/// @brief Parmi les 8 noeuds enfants d'un OctreeNode, retourne celui qui contient le point recherché
/// Version rapide, ne parcourt pas les enfants. A la place, calcul l'index de la boîte enfant
/// correspondante grâce à la fonction compareWithCenter
///@param root Le noeud parent
/// @param vert Le sommet recherché
/// @return Le noeud enfant contenant le sommet
OctreeNode& findBoundingBoxOfVertex_Optimized(OctreeNode &root, Point3 &vert) {
	int index = 0;
	
    std::vector<bool> decisions = compareWithCenter(vert, root.boundingBox);
    if (decisions[0]) index++;
	if (decisions[1]) index += 4;
	if (decisions[2]) index +=2;	

	return root.enfants.at(index);
}

/// @brief Ajoute un sommet à l'octree
/// @param cour La racine de l'octree
/// @param vert Le sommet à ajouter
void addVertexToOctree(OctreeNode &cour, Point3 &vert)
{
	// Pas une feuille
	if (cour.enfants.size() != 0) {
		OctreeNode& destination = findBoundingBoxOfVertex_Optimized(cour, vert);
		//OctreeNode& destination = findBoundingBoxOfVertex_Basic(cour, vert); // Recherche alternative, moins rapide
		addVertexToOctree(destination, vert);
	}
	// Feuille
	else {
		if (cour.liste_sommets.size() >= MAX_POINT && cour.depth < MAX_DEPTH) {
			addOctreeLevel(cour); // Ajouter 8 fils

			// Descendre sommet courant
			OctreeNode& destination = findBoundingBoxOfVertex_Optimized(cour, vert);
			//OctreeNode& destination = findBoundingBoxOfVertex_Basic(cour, vert); // Recherche alternative, moins rapide
			addVertexToOctree(destination, vert);

			// Descendre tous les sommets
			for(int i = 0; i < cour.liste_sommets.size(); i++) {
				OctreeNode& destination = findBoundingBoxOfVertex_Optimized(cour, cour.liste_sommets.at(i));
				//OctreeNode& destination = findBoundingBoxOfVertex_Basic(cour, cour.liste_sommets.at(i)); // Recherche alternative, moins rapide
				addVertexToOctree(destination, cour.liste_sommets.at(i));
			}
			
			// Supprimer les duplicatas des sommets descendus
			cour.liste_sommets.clear();
			
		}
		else {
			cour.liste_sommets.push_back(vert);
		}
	}
}

/// @brief A function to generate an octree of the vertices of a mesh,
/// Each vertex will be stored in a node of the octree.
/// the octree shall follow two rules:
///    1- each node shall only contain MAX_POINT vertices
///    2- the depth of tree shall not exceed MAX_DEPTH
///	Remark: the depth of the root is 0
///	Remark: rule 2 wins over rule 1.
/// i.e. a node may contain more vertices than MAX_POINT if the maximum depth is reached.
/// @param mesh the mesh of interest
/// @return an octree node that is the root of the octree for the given mesh
OctreeNode generateOctree(Polyhedron &mesh)
{
	std::cout << "========= Constructing octree =========" << std::endl;
	OctreeNode root;
    root.boundingBox = computeBB(mesh);

    for(Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); i++) {
        addVertexToOctree(root, i->point());
    }
	return root;
}

/// FONCTION NON UTILISEE DANS LE PROJET
/// @brief (optional) Utility function that takes an octree and apply a function (or more useful, a lambda !)
/// to each leaf of the Octree (each node containing vertices).
/// Can be useful to avoid declaring a new recursive function each time...
/// @param root the root node of the Octree of interest
/// @param func a lambda supposed to do something on a given Octree node.
void browseNodes(const OctreeNode &root, std::function<void(const OctreeNode&)> func)
{
	// if there are no vertices in the node we do nothing

	// if the nodes contains vertices, then "func" is called on the node
	if(root.liste_sommets.size() > 0) {
		func(root);
	}

	for (OctreeNode node : root.enfants) {
		browseNodes(node, func);
	}
}






//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Affichage d'un octree ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

/// @brief Remplit un tableau des bounding boxes correspondant aux feuilles de l'octree
/// Effectue à la volée la numérotation des noeuds.
/// @param root L'octree dont on veut récupérer les feuilles
/// @param boundingBoxes Le tableau à remplir
void getLeafBoundingBoxes(OctreeNode &root, std::vector<AABB> &boundingBoxes ){
	OctreeNode cour = root;

	if(cour.enfants.size() > 0) {
		for(OctreeNode it : cour.enfants) {
			getLeafBoundingBoxes(it,boundingBoxes);
		}
	}
	else if (cour.liste_sommets.size() > 0) {
		boundingBoxes.push_back(cour.boundingBox);
	}
}

/// @brief Pour une bounding box donnée, retourne tous les points qui la constituent, dans un tableau
/// @param boundingBox La bounding box en entrée
/// @return Le tableau contenant les points de la AABB
std::vector<Point3> getPointsOfAABB(AABB boundingBox){
	std::vector<Point3> resultat;
	resultat.push_back(boundingBox.coin_min);
	resultat.push_back(Point3{boundingBox.coin_max.x(), boundingBox.coin_min.y(), boundingBox.coin_min.z()});
	resultat.push_back(Point3{boundingBox.coin_min.x(), boundingBox.coin_max.y(), boundingBox.coin_min.z()});
	resultat.push_back(Point3{boundingBox.coin_max.x(), boundingBox.coin_max.y(), boundingBox.coin_min.z()});
	resultat.push_back(Point3{boundingBox.coin_min.x(), boundingBox.coin_min.y(), boundingBox.coin_max.z()});
	resultat.push_back(Point3{boundingBox.coin_max.x(), boundingBox.coin_min.y(), boundingBox.coin_max.z()});
	resultat.push_back(Point3{boundingBox.coin_min.x(), boundingBox.coin_max.y(), boundingBox.coin_max.z()});
	resultat.push_back(boundingBox.coin_max);

	return resultat;
}

/// @brief Ajoute les points formant une bounding box dans un fichier OFF
/// @param box La boîte à écrire dans le fichier
/// @param file Le fichier destination
void writeAABBVerticesToOFF(AABB box, std::ofstream &file) {
	std::vector<Point3> pointsOfBB = getPointsOfAABB(box);

	for(auto p : pointsOfBB) {
		file << p.x() << " " << p.y() << " " << p.z() << std::endl;
	}
}

/// @brief Ajoute les faces formant une bounding box dans un fichier OFF
/// @param index Le numéro de la boîte englobante parmi toutes les boîtes
/// @param file Le fichier destination
void writeAABBFacetsToOFF(int index, std::ofstream &file) {
	file << "4 " << 0 + 8 * index << " " << 2 + 8 * index << " " << 3 + 8 * index << " " << 1 + 8 * index << std::endl;
	file << "4 " << 4 + 8 * index << " " << 6 + 8 * index << " " << 2 + 8 * index << " " << 0 + 8 * index << std::endl;
	file << "4 " << 5 + 8 * index << " " << 7 + 8 * index << " " << 6 + 8 * index << " " << 4 + 8 * index << std::endl;
	file << "4 " << 1 + 8 * index << " " << 3 + 8 * index << " " << 7 + 8 * index << " " << 5 + 8 * index << std::endl;
	file << "4 " << 3 + 8 * index << " " << 2 + 8 * index << " " << 6 + 8 * index << " " << 7 + 8 * index << std::endl;
	file << "4 " << 0 + 8 * index << " " << 1 + 8 * index << " " << 5 + 8 * index << " " << 4 + 8 * index << std::endl;
}

/// @brief Crée un mesh (dans un fichier OFF) pour représenter un octree
/// sous la forme d'un ensemble de boîtes
/// @param boundingBoxesFeuilles Tableau contenant toutes les boîtes à ajouter au mesh
/// @param filePath Le fichier destination
void writeOctreeToOFF(std::vector<AABB> boundingBoxesFeuilles, std::string filePath) {
	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "OFF" << std::endl // "COFF" makes the file support color informations
			  << (boundingBoxesFeuilles.size() * 8) << ' '
			  << (boundingBoxesFeuilles.size() * 6) << " 0" << std::endl;

	for(int i = 0; i < boundingBoxesFeuilles.size(); i++) {
		writeAABBVerticesToOFF(boundingBoxesFeuilles.at(i), in_myfile);
	}

	for(int i = 0; i < boundingBoxesFeuilles.size(); i++) {
		writeAABBFacetsToOFF(i, in_myfile);
	}

	in_myfile.close();
}






//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Simplification d'un mesh ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

// Helper variable to construct the merged octree
int curr_index = 0;

/// @brief Pour chaque feuille d'un octree, calcule son point central à partir de 
/// tous les points présents dans la bounding box.
/// Remplit un tableau stockant tous les points centraux
/// @param root L'octree considéré
/// @param result Le tableau à remplir
void mergeVerticesInLeafs(OctreeNode& root, std::vector<Point3>& result) {
	if(root.enfants.size() > 0) {
		for(OctreeNode& it : root.enfants) {
			mergeVerticesInLeafs(it, result);
		}
	}
	else if (root.liste_sommets.size() > 0) {
		int i;
		Point3 mergedVertex = Point3(0, 0, 0);
		for (i = 0; i < root.liste_sommets.size(); i++) {
			mergedVertex = Point3(root.liste_sommets.at(i).x(), root.liste_sommets.at(i).y(), root.liste_sommets.at(i).z());
		}
		mergedVertex = Point3(mergedVertex.x() / i, mergedVertex.y() / i, mergedVertex.z() / i);

		root.mergedOctree_vertex = mergedVertex;
		result.push_back(mergedVertex);
		root.simplifiedOctree_index = curr_index;
		curr_index++;
	}
}

/// @brief Trouve parmi toutes les feuilles de l'octree, laquelle contient le sommet recherché
/// Cette fonction doit être appelée uniquement si les noeuds de l'octree ont un index valide.
/// @param root La racine de l'octree
/// @param vert Le sommet recherché
/// @return Le numéro du noeud contenant le sommet
int findLeafContainingVertex(OctreeNode& root, Polyhedron::Vertex_handle& vert) {
	if (root.enfants.size() > 0) {
		for (auto child : root.enfants)
		{
			int index = findLeafContainingVertex(child, vert);
			if (index != -1) return index;
		}
	}
	else {
		if (isVertexInBoundingBox(vert, root.boundingBox)) {
			return root.simplifiedOctree_index; // Vertice trouvé
		}
	}
	return -1;
}

/// @brief Crée un mesh (dans un fichier OFF) pour représenter un mesh simplifié à partir d'un octree
/// @param originalOctree L'octree servant à simplifier le mesh
/// @param mesh Le mesh à simplifier
/// @param filePath Le chemin du fichier destination
void writeSimplifiedMeshToOFF(OctreeNode &originalOctree, Polyhedron &mesh, std::string filePath)
{
	std::ofstream in_myfile;
	in_myfile.open(filePath);

	std::ostringstream facets_buffer;

	CGAL::set_ascii_mode(in_myfile);

	// Gets a merged octree from original one, and a list of vertices to add to the simplified mesh
	std::vector<Point3> mergedOctreeVertices;
	OctreeNode mergedOctree = originalOctree;
	mergeVerticesInLeafs(mergedOctree, mergedOctreeVertices);

	std::cout << "========= Constructing facets of the simplified mesh =========" << std::endl;
	int nbFacets = 0;
	unsigned int nbFacetsInMesh = 0;
	unsigned int current_facet = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		++nbFacetsInMesh;
	}

	for (Facet_iterator f = mesh.facets_begin(); f != mesh.facets_end(); ++f)
	{
		// Gets the first vertex of the current face
		Polyhedron::Vertex_handle v1 = f->halfedge()->vertex();
		// Finds which leaf of the octree contains this vertex
		int node1 = findLeafContainingVertex(mergedOctree, v1);

		// Second vertex
		Polyhedron::Vertex_handle v2 = f->halfedge()->next()->vertex();
		int node2 = findLeafContainingVertex(mergedOctree, v2);

		// Third vertex
		Polyhedron::Vertex_handle v3 = f->halfedge()->next()->next()->vertex();
		int node3 = findLeafContainingVertex(mergedOctree, v3);

		// La face est un carré
		if (f->is_quad()) {
			Vertex_iterator v4 = f->halfedge()->next()->next()->next()->vertex();
			int node4 = findLeafContainingVertex(mergedOctree, v4);

			if (node1 != node2 && node1 != node3 && node1 != node4 && node2 != node3 && node2 != node4 && node3 != node4) {
				facets_buffer << "4 " << node1 << " " << node2 << " " << node3 << " " << node4 << " \n";
				nbFacets++;
			}
		}
		// La face est un triangle
		else {
			if (node1 != node2 && node1 != node3 && node2 != node3) {
				if(node1 != -1 && node2 != -1 && node3 != -1) {
					facets_buffer << "3 " << node1 << " " << node2 << " " << node3 << std::endl;
					nbFacets++;
					std::cout << "Constructing facet : " << current_facet << " / " << nbFacetsInMesh << std::endl;
				}
				else {
					std::cerr << "Error : one of the vertices has not been found in the octree. Skipping current facet." << std::endl;
				}
			}
		}

		current_facet++;
	}

	// Writes general information about the mesh
	in_myfile << "OFF" << std::endl // "COFF" makes the file support color informations
			  << mergedOctreeVertices.size() << ' '
			  << nbFacets << " 0" << std::endl;

	// Adds all vertices from merged octree
	std::cout << "========= Adding vertices to OFF file =========" << std::endl;
	unsigned int nbVertices = mergedOctreeVertices.size();
	int nbAdded = 0;
	for(auto p : mergedOctreeVertices) {
		in_myfile << p.x() << " " << p.y() << " " << p.z() << std::endl;

		nbAdded++;
		std::cout << "Adding vertex : " << nbAdded << " / " << nbVertices << std::endl;
	}
	
	std::cout << "========= Adding constructed facets to OFF file =========" << std::endl;
	in_myfile << facets_buffer.str();

	in_myfile.close();

	std::cout << "\nLe résultat a été exporté dans " << filePath << " !" << std::endl;
}






//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////// Programme principal /////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

/// @brief Programme principal
/// @param argc Nombre de paramètres attendus
/// @param argv Paramètres attendus :
///		- chemin du fichier OFF
///		- nb. points max par feuille de l'arbre
///		- profondeur max de l'arbre
int main(int argc, char *argv[])
{
	// Vérification des paramètres
	if (argc < 4)
	{
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off, ainsi que le nombre de sommets max par feuille de l'octree, puis la profondeur max de l'octree." << std::endl;
		return 1;
	}

	Polyhedron mesh;
	std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty())
	{
		std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
		return 1;
	}

	// Conversion du paramètre nbSommets en int
	std::istringstream ss(argv[2]);
	int nbSommets;
	if (!(ss >> nbSommets) || !ss.eof()) {
		std::cerr << "Le nombre de sommets donné est invalide : " << argv[2] << '\n';
		return 1;
	}
	MAX_POINT = nbSommets;

	// Conversion du paramètre profondeur en int
	std::istringstream ss2(argv[3]);
	int profondeur;
	if (!(ss2 >> profondeur) || !ss2.eof()) {
		std::cerr << "La profondeur donnée est invalide : " << argv[3] << '\n';
		return 1;
	}
	MAX_DEPTH = profondeur;

	std::srand(std::time(nullptr));

	// Récupération des infos du mesh : nb verts, nb faces, nb arêtes
	unsigned int nbVerts = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i)
	{
		++nbVerts;
	}
	std::cout << "Nombre de sommets: " << nbVerts << std::endl;

	unsigned int nbEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i)
	{
		++nbEdges;
	}
	nbEdges /= 2;
	std::cout << "Nombre d'arêtes: " << nbEdges << std::endl;

	unsigned int nbFaces = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		++nbFaces;
	}
	std::cout << "Nombre de faces: " << nbFaces << std::endl;

	// Début du traitement

	// Génération de l'octree
	OctreeNode octree = generateOctree(mesh);

	// Affichage de l'octree (dans un fichier OFF)
	std::vector<AABB> boundingBoxesFeuilles;
	getLeafBoundingBoxes(octree, boundingBoxesFeuilles); // Numérote aussi les noeuds pour la simplification
	writeOctreeToOFF(boundingBoxesFeuilles, "octree.off");

	// Simplification du mesh à partir de l'octree (dans un autre fichier OFF)
	writeSimplifiedMeshToOFF(octree, mesh, "simplified.off");

	return 0;
}
